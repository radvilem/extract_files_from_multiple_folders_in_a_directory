import glob, os, shutil
import fnmatch

def copy_files(my_dir, save_dir, file_pattern):

    for dirpath, dirs, files in os.walk(my_dir):
        for filename in fnmatch.filter(files, file_pattern):
            # print(os.path.join(dirpath, filename))
            shutil.copy2(os.path.join(dirpath, filename), save_dir)


copy_files ("path/to/direcoty", "path/to/save/directory", "*.nii.gz")
